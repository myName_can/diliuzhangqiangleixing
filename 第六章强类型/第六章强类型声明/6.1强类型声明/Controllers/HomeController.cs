﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _6._1强类型声明.Models;

namespace _6._1强类型声明.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            using (fashionshoppingDBEntities db = new fashionshoppingDBEntities())
            {
                return View(db.Procates.ToList());
            }
        }
    }
}