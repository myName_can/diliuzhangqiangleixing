﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 强类型_1.Models;

namespace 强类型_1.Controllers
{
    public class DefaultController : Controller
    {
        private fashionshoppingDBEntities _fdb;
        public DefaultController()
        {
            _fdb = new fashionshoppingDBEntities();
        }

        // GET: Default
        public ActionResult Index()
        {
            var table = _fdb.Products.Include("Procate").ToList();
            return View(table);
        }


        // GET: Default
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
            {
                User login = _fdb.Users.FirstOrDefault(u=>u.email==user.email && u.pwd==user.pwd);

                if (login == null)
                {
                    ModelState.AddModelError("","邮箱或密码错误！");
                }
                else
                {
                    return RedirectToAction("Index", "Default");
                }
            }
            return View();
        }
    }
}